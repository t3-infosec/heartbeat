class Note {
    /**
     * @param {string} pitch
     * @param {number} octave
     * @param {number} time
     * @param {number} duration
     */
    constructor(pitch, octave, time, duration) {
        this.pitch = pitch;
        this.octave = octave;
        this.duration = duration;
        this.time = time;
    }
}

class Sequence {
    /**
     * @param {Note[]} notes
     */
    constructor(notes) {
        this.notes = notes;
    }

    hasNote(index) {
        return index < this.notes.length;
    }

    note(index) {
        return this.notes[index];
    }

    get length() {
        return this.notes.length;
    }
}

class Phrase {
    /**
     * @param {Sequence[]} sequences
     */
    constructor(sequences) {
        this.sequences = sequences;
    }

    hasSequence(index) {
        return index < this.sequences.length;
    }

    sequence(index) {
        return this.sequences[index];
    }

    get length() {
        return this.sequences.length;
    }
}

class Player {
    /**
     * @param {Phrase[]} phrases
     * @param {number} speed
     */
    constructor(speed, phrases) {
        this.speed = speed;
        this.phrases = phrases;
        this.correctMessage = document.querySelector('#correctMessage');
        this.incorrectMessage = document.querySelector('#incorrectMessage');

        this.pwEl = document.querySelector('#password');
        this.pwEl.addEventListener('input', (evt) => {
            if (evt.data !== null) {
                this.playNextSequence();
            }
            if (this.pwEl.value === '') {
                this.reset();
            }
            console.log(this.pwEl.value.length);
        });

        this.resetBtn = document.querySelector('[type=reset]');
        this.resetBtn.addEventListener('click', (evt) => {
            this.reset();
            this.pwEl.focus();
            this.incorrectMessage.style.display = 'none';
            this.correctMessage.style.display = 'none';
        });

        this.submitBtn = document.querySelector('#submit');
        this.submitBtn.addEventListener('click', (evt) => {
            if (this.pwEl.value === "butafufifrduoccrstpogenumahutrshdiscgaprswdrbido") {
                this.correctMessage.style.display = 'block';
            } else {
                this.incorrectMessage.style.display = 'block';
            }
        });

        this.piano = Synth.createInstrument('piano');
        this.reset();
    }

    playNextSequence() {
        const phrase = this.phrases[this.phraseIndex];
        const sequence = phrase.sequence(this.sequenceIndex);

        let timeout = 0;
        while (sequence.hasNote(this.noteIndex)) {
            const note = sequence.note(this.noteIndex++);
            setTimeout(() => this.play(note), timeout);
            timeout += note.time * this.speed;
        }
        this.noteIndex = 0;

        if (phrase.hasSequence(this.sequenceIndex + 1)) {
            this.sequenceIndex++;
        } else {
            this.phraseIndex++;
            this.sequenceIndex = 0;
        }

        if (this.pwEl.value.length % 12 === 0) {
            this.playToTheEnd(phrase);
        }
    }

    playToTheEnd(phrase) {
        let timeout = 0;
        while (this.sequenceIndex < phrase.length) {
            const sequence = phrase.sequence(this.sequenceIndex);
            while (sequence.hasNote(this.noteIndex)) {
                const note = sequence.note(this.noteIndex++);
                timeout += note.time * this.speed;
                setTimeout(() => this.play(note), timeout);
            }
            this.noteIndex = 0;
            this.sequenceIndex++;
        }
        this.sequenceIndex = 0;
        this.phraseIndex++;

        if (this.phraseIndex === this.phrases.length) {
            this.reset();
        }
    }

    play(note) {
        this.piano.play(note.pitch, note.octave, note.duration);
    }

    reset() {
        this.phraseIndex = 0;
        this.sequenceIndex = 0;
        this.noteIndex = 0;
    }

}

const player = new Player(500, [
    new Phrase([
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('C', 5, 1, 2)]),
        new Sequence([new Note('D', 5, 1, 2)]),
        new Sequence([new Note('D', 5, 1, 2)]),
        new Sequence([new Note('C', 5, 1, 2)]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 2)]),
        new Sequence([new Note('G', 4, 1, 2)]),
        new Sequence([new Note('G', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 2)]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('B', 4, 1, 3)]),
        new Sequence([
            new Note('A', 4, 1.5, 1),
            new Note('A', 4, 0.5, 4),
        ]),
    ]),
    new Phrase([
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('C', 5, 1, 2)]),
        new Sequence([new Note('D', 5, 1, 2)]),
        new Sequence([new Note('D', 5, 1, 2)]),
        new Sequence([new Note('C', 5, 1, 2)]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 2)]),
        new Sequence([new Note('G', 4, 1, 2)]),
        new Sequence([new Note('G', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 2)]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 3)]),
        new Sequence([
            new Note('G', 4, 1.5, 1),
            new Note('G', 4, 0.5, 4),
        ]),
    ]),
    new Phrase([
        new Sequence([new Note('A', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 2)]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('G', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 2)]),
        new Sequence([
            new Note('B', 4, 0.5, 1.2),
            new Note('C', 5, 0.5, 1.2)
            ]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('G', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 2)]),
        new Sequence([
            new Note('B', 4, 0.5, 1.2),
            new Note('C', 5, 0.5, 1.2)
            ]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 2)]),
        new Sequence([new Note('G', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 3)]),
        new Sequence([new Note('D', 5, 1, 2)]),
    ]),
    new Phrase([
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('C', 5, 1, 2)]),
        new Sequence([new Note('D', 5, 1, 2)]),
        new Sequence([new Note('D', 5, 1, 2)]),
        new Sequence([new Note('C', 5, 1, 2)]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 2)]),
        new Sequence([new Note('G', 4, 1, 2)]),
        new Sequence([new Note('G', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 2)]),
        new Sequence([new Note('B', 4, 1, 2)]),
        new Sequence([new Note('A', 4, 1, 3)]),
        new Sequence([
            new Note('G', 4, 1.5, 1),
            new Note('G', 4, 0.5, 4),
        ]),
    ]),
]);