# Heartbeat
Heartbeat is a log-in format that synchronizes the user's passoword typing to a popular melody. As a result, the user is induced to type in a standard, predictable rhythm, which serves as protection against acoustic side-channel attacks (when someone discovers your passoword by eavesdropping your keyboard noises).

### Is this kind of attack an actual hazard, though?
Definitely!

[This research paper](https://www.researchgate.net/publication/220593625_Keyboard_acoustic_emanations_revisited) introduces an attack that could recover up to 96% of typed characters after a 10-minute recording of the user typing English text using a keyboard. In the experiments, 90% of 5-character random passwords using only letters could be generated in fewer than 20 attempts by an adversary, and 80% of 10-character passwords could be generated in fewer than 75 attempts.

Furthermore, [a group of researchers](https://dl.acm.org/doi/10.1145/3359789.3359816) developed a deep learning system that identifies English characters from the sound of typing with an error rate of only 7.41% for known typists and 15.41% for unknown typists; in addition, it can also adapt to noisy places.

Lastly,
[another article](https://www.semanticscholar.org/paper/Keyboard-acoustic-side-channel-attacks%3A-exploring-Halevi-Saxena/e2eed399d50e1e0bbdee8f9e22a24177f7e106a8) explores a model that could reduce the entropy of the search space by up to 57% per character.


### How can I try it?
Follow [this link](https://t3-infosec.gitlab.io/heartbeat/) and simply type the password; as you press each key, a piano note to a popular melody will be emitted. Try to play it in the correct tempo and feel like a pianist in the meantime! Oh, be sure to wait a few moments at the end of each passphrase for the last notes to be played; otherwise you might face some unharmonious collisions.

### How is this format implemented?
In addition to the basic HTML, CSS, and Javascript mechanics for a simple password-checking page, our approach employs two key elements:

1. An object-oriented architecture that divides each melody into Player, Phrase, Sequence, and Note classes;
2. The AudioSynth Javascript library to generate the piano notes.

Each melody is contained inside a Player (for now, only _Ode to Joy_ is available). Each passphrase composing the password is associated with a musical phrase. At each key press, a Sequence is triggered. This Sequence is composed of at least one Note generated by the piano functions in the AudioSynth Javascript library. At the end of each passphrase, a special Sequence plays a few Notes automatically to finish the current verse.

### Credits
This work has been brought to you by Onyxcorp.

- Ideation: Yuri S Villas Boas and Elisa Flemer
- Management: Yuri S Villas Boas
- Software development: Elisa Flemer and anonymous
- Documentation: Elisa Flemer
